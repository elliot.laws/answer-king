import { EventAggregator } from 'aurelia-event-aggregator';
import { bindable, autoinject } from 'aurelia-framework';

@autoinject
export class ProductCardCustomElement {
  @bindable
  public item: IItem;

  constructor(private ea: EventAggregator) {}

  addToOrder(item: IItem) {

    
    console.log(`${item.title} has been added to your order`);
    this.ea.publish('addToOrder', { item: item });
  }
}
