import { autoinject, bindable } from 'aurelia-framework';
import { CategoryService } from 'services/category/category-service';
import { ItemService } from 'services/item/item-service';
import { EventAggregator } from 'aurelia-event-aggregator';

@autoinject
export class OrderSelectionLayout {
  constructor(
    private categoryService: CategoryService,
    private itemService: ItemService,
    ea: EventAggregator
  ) {
    ea.subscribe('categoryIdSelected', payload => {
      console.log(`Category id selected is: ${payload.categoryId}`);
      this.filterItemsSelectedCatergory(payload.categoryId);
    });
  }

  public categories: ICategory[] = [];
  public items: IItem[] = [];
  public filtereditems: IItem[] = [];

  // since this class is handled by the Router,
  // it has an activate method which receives the
  // route parameters.
  public activate(params) {
    //this.orderId = params.id;
  }

  public async attached() {
    this.categories = await this.categoryService.getCategories();
    console.log(this.categories);

    this.items = await this.itemService.getItems();
    console.log(this.items);

    const firstCategory = 0;

    if (firstCategory === 0) {
      this.filterItemsSelectedCatergory(firstCategory);
    }
  }

  public filterItemsSelectedCatergory(catId) {
    if (catId === 0) {
      this.filtereditems = this.items;
    } else {
      this.filtereditems = this.items.filter(item =>
        item.categories.some(c => c.id === catId)
      );
      console.log(this.filtereditems);
    }
  }
}
