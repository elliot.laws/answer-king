import { autoinject, bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

@autoinject
export class CategoriesList {
  @bindable
  public categories: ICategory[];

  selectedCatId: number;

  constructor(private ea: EventAggregator) {}

  public async attached() {}

  categorySelect(categoryId: number) {
    this.selectedCatId = categoryId;
    this.ea.publish('categoryIdSelected', { categoryId: categoryId });
  }
}
