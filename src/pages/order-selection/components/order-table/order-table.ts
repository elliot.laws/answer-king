import { OrdersService } from 'services/orders/orders-service';
import { autoinject, bindable } from 'aurelia-framework';
import { EventAggregator, Subscription } from 'aurelia-event-aggregator';
import { isReturnStatement } from '@babel/types';

@autoinject
export class OrderTableCustomElement {
  @bindable
  public orderItems: ILineItem[] = [];
  public totalPrice: number;
  canCheckout:boolean = false;

  constructor(private ea: EventAggregator, private orderService: OrdersService) {}

  private subscriptions: Subscription[] = [];

  public attached() {
    //this.subscriptions.push(this.ea.subscribe('addToOrder', payload => payload.item));

    this.ea.subscribe('addToOrder', payload => {
      this.itemAdded(payload.item);
    })
  }

  public detached() {
    this.subscriptions.forEach(s => s.dispose());
  }

  private orderAdded(item: IItem) {
    console.log(item);
  }

  private async itemAdded(item: IItem) {
    const itemId: IItemId = item.id as unknown as IItemId;

    if (this.orderItems.length === 0) {
      // create new order with item id 
      //const orderId = await this.orderService.createOrder(itemId);
      //console.log('order created with id: ' + orderId);
    } else {
      // update order
    }

    let dupItem = false;
    
    // If an item has already been added 
    // then check for duplicate items and update quantities
    if (this.canCheckout) {
      this.orderItems.forEach(i => {
        if (item.id === i.itemId) {
          i.quantity++;
          i.subtotal += item.price

          dupItem = true;
        } 
      });
    }
    
    // If a duplicate item has not been found
    // then create a new record in order items array
    if (!dupItem) {
      const lineItem: ILineItem = {
        itemId: item.id,
        itemName: item.title,
        quantity: 1,
        subtotal: item.price
      };
  
      this.orderItems.push(lineItem);
    }

    this.updateTotalPrice();
    this.canCheckout = true;
  }

  private updateTotalPrice() {
    this.totalPrice = this.orderItems.reduce((a, b) => a + b.subtotal, 0)
  }
}

