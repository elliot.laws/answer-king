import {autoinject, bindable} from 'aurelia-framework';
import { ItemService } from 'services/item/item-service';

interface Contact {
  firstName: string;
  lastName: string;
  email: string;
}

@autoinject
export class ItemsList {
  @bindable
  public filtereditems: IItem[] = [];

  constructor(private itemService: ItemService) {
    console.log(this.filtereditems);
  }

  public async activate() {
    console.log(this.filtereditems);
  }

  save() {
    // call service to add item to order
  }
}

