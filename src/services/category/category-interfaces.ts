interface ICategory {
  id: number;
  title: string;
  items: IItemCategory[];
  description: string;
}

interface IItemCategory {
  id: number;
  title: string;
  price: number;
  description: string;
  validUntil: Date;
}

/**
 * This is the model that the API expects when
 * updating a category.
 */
interface ICategoryUpdate {
  name: string;
  description: string;
}

interface ICategoryId {
  id: string;
}
