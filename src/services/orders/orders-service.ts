import { autoinject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';

@autoinject
export class OrdersService {
  constructor(private http: HttpClient) {}

  public async createOrder(itemId: IItemId): Promise<string> {
    const body: IOrderCreate = { 
      items: [itemId]
    };

    const response = await this.http.post('order', json(body));

    if (!response.ok) {
      return null;
    }

    // We need to await the deserialisation
    // of the response body.
    const order: IOrder = await response.json();
    
    console.log(order.id);
    return order.id;
  }
}
