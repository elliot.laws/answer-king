interface IItem {
  id: number;
  title: string;
  price: number;
  description: string;
  validUntil: Date;
  categories: ICategoryItem[];
}

interface ILineItem {
  itemId: number,
  itemName: string;
  quantity: number;
  subtotal: number;
}


interface ICategoryItem {
  id: number;
  title: string;
  description: string;
}

interface IItemUpdate {
  name: string;
  description: string;
  price: number;
  category: ICategoryId;
}

interface IItemId {
  id: string;
}
