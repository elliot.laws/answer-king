import { autoinject, PLATFORM } from 'aurelia-framework';
import { RouterConfiguration } from 'aurelia-router';

import 'scss/styles.scss';

@autoinject
export class App {
  public configureRouter(config: RouterConfiguration) {
    config.title = '👑 ANSWER KING';

    config.map([
      {
        route: ['', '/'],
        redirect: 'welcome',
      },
      {
        route: 'welcome',
        name: 'welcome',
        title: 'Welcome',
        moduleId: PLATFORM.moduleName('pages/welcome/welcome-layout'),
      },
      {
        route: 'order',
        name: 'order',
        title: 'Order',
        moduleId: PLATFORM.moduleName('pages/order-selection/order-selection-layout'),
      },
    ]);
  }
}
